mod tokenizer;
use tokenizer::Token as tok;
use tokenizer::Tokenizer as tkizer;

pub enum ExprType {}

pub struct AST {}

pub struct Parser {
    tokenizer: tkizer,
    cur_tok: tok,
}

impl Parser {
    pub fn new(tokenizer: tkizer) {
        Parser {
            tokenizer: tkizer,
            cur_tok: tkizer.tokenize_next(),
        }
    }
}
