mod tokenizer;
use std::env; // for command line args
use std::fs::File; // importing file class
use std::io::prelude::*; // for reading from files

use tokenizer::TokenType as ttype;
use tokenizer::Tokenizer as tkizer;

fn run_compiler() -> i32 {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];

    let mut file = File::open(filename).expect("Unable to open the file");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Unable to read the file");

    let borrowed_string: &str = "\0";
    contents.push_str(borrowed_string);

    let mut tokenizer = tkizer::new(contents);
    let mut tok = tokenizer.tokenize_next();
    while !tokenizer.is_done() {
        if tok.get_type() as i32 == ttype::Error as i32 {
            eprintln!("Error on line {}:", tok.get_line_number());
            eprintln!("{}", tok.get_value());
            return 1;
        }

        println!("token: {}", tok.display());
        println!("value: {}\n", tok.get_value());
        tok = tokenizer.tokenize_next();
    }
    0
}

fn main() {
    let exit_code = run_compiler();
    std::process::exit(exit_code);
}
