pub struct Tokenizer {
    source: String,
    i: usize,
    c: char,
    line_num: usize,
    done: bool,
}

#[derive(Clone)]
pub enum TokenType {
    LParen,
    RParen,
    LBrace,
    RBrace,
    LBracket,
    RBracket,
    SemiColon,
    Equals,
    Comma,
    Period,
    QuestionMark,
    PointerArrow,
    ReturnArrow,
    MathOperator,
    ReassignOperator,
    LogicOperator,
    Not,
    Keyword,
    ID,
    Character,
    String,
    IntegerLiteral,
    FloatLiteral,
    Comment,
    Error,
    Eof,
}

pub struct Token {
    t: TokenType,
    value: String,
    line_num: usize,
}

impl Token {
    pub fn new(t: TokenType, value: String, line_num: usize) -> Token {
        Token { t, value, line_num }
    }

    pub fn display(&mut self) -> String {
        match self.t {
            TokenType::LParen => "LParen".to_string(),
            TokenType::RParen => "RParen".to_string(),
            TokenType::LBrace => "LBrace".to_string(),
            TokenType::RBrace => "RBrace".to_string(),
            TokenType::LBracket => "LBracket".to_string(),
            TokenType::RBracket => "RBracket".to_string(),
            TokenType::SemiColon => "SemiColon".to_string(),
            TokenType::Equals => "Equals".to_string(),
            TokenType::Comma => "Comma".to_string(),
            TokenType::Period => "Period".to_string(),
            TokenType::QuestionMark => "QuestionMark".to_string(),
            TokenType::PointerArrow => "PointerArrow".to_string(),
            TokenType::ReturnArrow => "ReturnArrow".to_string(),
            TokenType::MathOperator => "MathOperator".to_string(),
            TokenType::ReassignOperator => "ReassignOperator".to_string(),
            TokenType::LogicOperator => "LogicOperator".to_string(),
            TokenType::Not => "Not".to_string(),
            TokenType::Keyword => "Keyword".to_string(),
            TokenType::ID => "ID".to_string(),
            TokenType::Character => "Character".to_string(),
            TokenType::String => "String".to_string(),
            TokenType::IntegerLiteral => "IntegerLiteral".to_string(),
            TokenType::FloatLiteral => "FloatLiteral".to_string(),
            TokenType::Comment => "Comment".to_string(),
            TokenType::Error => "Error".to_string(),
            TokenType::Eof => "EOF".to_string(),
        }
    }
    pub fn get_type(&mut self) -> TokenType {
        self.t.clone()
    }

    pub fn get_line_number(&mut self) -> usize {
        self.line_num
    }

    pub fn get_value(&mut self) -> String {
        self.value.clone()
    }
}

impl Tokenizer {
    pub fn new(source: String) -> Tokenizer {
        Tokenizer {
            source: source.clone(),
            i: 0,
            c: source.chars().next().unwrap(),
            line_num: 1,
            done: false,
        }
    }

    pub fn is_done(&mut self) -> bool {
        self.done
    }

    fn advance_tokenizer(&mut self) {
        if self.i < self.source.chars().count() - 1 {
            if self.c == '\n' {
                self.line_num += 1;
            }
            self.i += 1;
            self.c = self.source.chars().nth(self.i).unwrap();
        }
    }

    fn skip_whitespace(&mut self) {
        while self.c.is_whitespace() && self.i < self.source.chars().count() - 1 {
            self.advance_tokenizer();
        }
    }

    fn ignore_comment(&mut self) {
        while self.c != '\n' {
            self.advance_tokenizer();
        }
        self.advance_tokenizer();
    }

    fn collect_char(&mut self) -> Token {
        let mut val: String = "".to_owned();
        self.advance_tokenizer();

        if self.c == '\\' {
            for _n in 1..3 {
                let str_char: String = self.c.to_string().to_owned();
                val.push_str(&str_char);
                self.advance_tokenizer();
            }
        } else {
            let str_char: String = self.c.to_string();
            val.push_str(&str_char);
            self.advance_tokenizer();
        }

        if self.c != '\'' {
            return Token::new(
                TokenType::Error,
                "A character literal must contain only one character.".to_string(),
                self.line_num,
            );
        }
        self.advance_tokenizer();
        Token::new(TokenType::Character, val, self.line_num)
    }

    fn collect_string(&mut self) -> Token {
        let mut val: String = "".to_owned();
        self.advance_tokenizer();
        let saved_line = self.line_num;
        while self.c != '"' {
            if self.source.chars().nth(self.i + 1).unwrap() == '\0' {
                return Token::new(
                    TokenType::Error,
                    "String never closes".to_string(),
                    saved_line,
                );
            }

            let str_char: String = self.c.to_string().to_owned();
            val.push_str(&str_char);
            self.advance_tokenizer();
        }
        self.advance_tokenizer();
        Token::new(TokenType::String, val, self.line_num)
    }

    fn collect_id(&mut self) -> Token {
        let mut val: String = "".to_owned();
        while self.c.is_alphanumeric() || self.c == '_' || self.c == '-' {
            let str_char: String = self.c.to_string().to_owned();
            val.push_str(&str_char);
            self.advance_tokenizer();
        }
        let keywords: [&str; 16] = [
            "for", "while", "func", "if", "else", "var", "struct", "switch", "case", "mut", "char",
            "float", "int", "null", "break", "continue",
        ];

        for keyword in keywords.iter() {
            if keyword.eq(&val) {
                return Token::new(TokenType::Keyword, val, self.line_num);
            }
        }

        Token::new(TokenType::ID, val, self.line_num)
    }

    fn collect_number(&mut self) -> Token {
        let mut val: String = "".to_owned();
        let mut is_float = false;
        while self.c.is_digit(10) || self.c == '.' && !is_float {
            if self.c == '.' {
                is_float = true;
            }

            let str_char: String = self.c.to_string().to_owned();
            val.push_str(&str_char);
            self.advance_tokenizer();
        }
        if is_float {
            return Token::new(TokenType::FloatLiteral, val, self.line_num);
        }
        Token::new(TokenType::IntegerLiteral, val, self.line_num)
    }

    fn collect_equals(&mut self) -> Token {
        self.advance_tokenizer();
        if self.c == '=' {
            self.advance_tokenizer();
            Token::new(TokenType::LogicOperator, "==".to_string(), self.line_num)
        } else if self.c == '>' {
            self.advance_tokenizer();
            Token::new(TokenType::ReturnArrow, "=>".to_string(), self.line_num)
        } else {
            Token::new(TokenType::Equals, "=".to_string(), self.line_num)
        }
    }

    fn collect_minus(&mut self) -> Token {
        self.advance_tokenizer();
        if self.c == '=' {
            self.advance_tokenizer();
            Token::new(TokenType::ReassignOperator, "-=".to_string(), self.line_num)
        } else if self.c == '>' {
            self.advance_tokenizer();
            Token::new(TokenType::PointerArrow, "->".to_string(), self.line_num)
        } else {
            Token::new(TokenType::MathOperator, "-".to_string(), self.line_num)
        }
    }

    fn collect_plus(&mut self) -> Token {
        self.advance_tokenizer();
        if self.c == '=' {
            self.advance_tokenizer();
            Token::new(TokenType::ReassignOperator, "+=".to_string(), self.line_num)
        } else {
            Token::new(TokenType::MathOperator, "+".to_string(), self.line_num)
        }
    }

    fn collect_multiply(&mut self) -> Token {
        self.advance_tokenizer();
        if self.c == '=' {
            self.advance_tokenizer();
            Token::new(TokenType::ReassignOperator, "*=".to_string(), self.line_num)
        } else {
            Token::new(TokenType::MathOperator, "*".to_string(), self.line_num)
        }
    }

    fn collect_divide(&mut self) -> Token {
        self.advance_tokenizer();
        if self.c == '=' {
            self.advance_tokenizer();
            Token::new(TokenType::ReassignOperator, "/=".to_string(), self.line_num)
        } else if self.c == '/' {
            self.advance_tokenizer();
            self.ignore_comment();
            Token::new(TokenType::Comment, String::new(), self.line_num)
        } else {
            Token::new(TokenType::MathOperator, "/".to_string(), self.line_num)
        }
    }

    fn collect_not(&mut self) -> Token {
        self.advance_tokenizer();
        if self.c == '=' {
            self.advance_tokenizer();
            Token::new(TokenType::LogicOperator, "!=".to_string(), self.line_num)
        } else {
            Token::new(TokenType::Not, "!".to_string(), self.line_num)
        }
    }

    fn collect_less_than(&mut self) -> Token {
        self.advance_tokenizer();
        if self.c == '=' {
            self.advance_tokenizer();
            Token::new(TokenType::LogicOperator, "<=".to_string(), self.line_num)
        } else {
            Token::new(TokenType::LogicOperator, "<".to_string(), self.line_num)
        }
    }

    fn collect_greater_than(&mut self) -> Token {
        self.advance_tokenizer();
        if self.c == '=' {
            self.advance_tokenizer();
            Token::new(TokenType::LogicOperator, ">=".to_string(), self.line_num)
        } else {
            Token::new(TokenType::LogicOperator, ">".to_string(), self.line_num)
        }
    }

    pub fn tokenize_next(&mut self) -> Token {
        if self.c.is_whitespace() {
            self.skip_whitespace();
        }

        if self.c == '\0' {
            self.done = true;
            return Token::new(TokenType::Eof, "End of file.".to_string(), self.line_num);
        }

        if self.c.is_alphabetic() {
            return self.collect_id();
        }

        if self.c.is_digit(10) {
            return self.collect_number();
        }

        match self.c {
            '(' => {
                self.advance_tokenizer();
                Token::new(TokenType::LParen, "(".to_string(), self.line_num)
            }
            ')' => {
                self.advance_tokenizer();
                Token::new(TokenType::RParen, ")".to_string(), self.line_num)
            }
            '[' => {
                self.advance_tokenizer();
                Token::new(TokenType::LBracket, "[".to_string(), self.line_num)
            }
            ']' => {
                self.advance_tokenizer();
                Token::new(TokenType::RBracket, "]".to_string(), self.line_num)
            }
            '{' => {
                self.advance_tokenizer();
                Token::new(TokenType::LBrace, "{".to_string(), self.line_num)
            }
            '}' => {
                self.advance_tokenizer();
                Token::new(TokenType::RBrace, "}".to_string(), self.line_num)
            }
            ';' => {
                self.advance_tokenizer();
                Token::new(TokenType::SemiColon, ";".to_string(), self.line_num)
            }
            ',' => {
                self.advance_tokenizer();
                Token::new(TokenType::Comma, ",".to_string(), self.line_num)
            }
            '.' => {
                self.advance_tokenizer();
                Token::new(TokenType::Period, ".".to_string(), self.line_num)
            }
            '?' => {
                self.advance_tokenizer();
                Token::new(TokenType::QuestionMark, "?".to_string(), self.line_num)
            }
            '<' => self.collect_less_than(),
            '>' => self.collect_greater_than(),
            '\'' => self.collect_char(),
            '"' => self.collect_string(),
            '=' => self.collect_equals(),
            '+' => self.collect_plus(),
            '-' => self.collect_minus(),
            '*' => self.collect_multiply(),
            '/' => self.collect_divide(),
            '!' => self.collect_not(),
            _ => {
                self.done = true;
                Token::new(
                    TokenType::Error,
                    format!("Undefined character {}", self.c),
                    self.line_num,
                )
            }
        }
    }
}
