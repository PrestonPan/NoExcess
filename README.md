# No Excess
This is a project I decided to redo because C is too much pain to write in.
Therefore, I am doing it in rust.

## Introduction
Basically, by the end this should be a complete language with a very small
standard library (I don't have the time to write one). Also, it _should_ support
pointers and arrays, but I might be too lazy and/or unskilled to write that. It will
be interpreted at first but will later be compiled if I ever have the willpower to write
a ton of x86-64 assembly.

## Building
To build, run `cargo build`. to run, `cargo run [file path]`.
